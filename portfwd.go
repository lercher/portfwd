package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"sync"
)

var (
	flagPort = flag.Int("p", 25, "port to forward")
	flagDest = flag.String("s", "", "`servername:port` to forward the port to")
)

func handleConnection(conn net.Conn, dest string, port, n int) {
	defer conn.Close()

	if !strings.Contains(dest, ":") {
		dest = fmt.Sprint(dest, ":", port)
	}
	log.Println(n, "wiring incomming connection to", dest, "...")
	addr, err := net.ResolveTCPAddr("tcp", dest)
	if err != nil {
		log.Println(n, err)
		return
	}
	fwd, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Println(n, err)
		return
	}
	defer fwd.Close()

	var mu sync.Mutex
	mu.Lock()
	go func() {
		defer mu.Unlock()

		_, err2 := io.Copy(conn, fwd)
		if err2 != nil {
			log.Println(n, "reading remote stream:", err2)
		}
		conn.Close()
		fwd.Close()
	}()

	_, err = io.Copy(fwd, conn)
	if err != nil {
		log.Println(n, "writing remote stream:", err)
	}
	conn.Close()
	fwd.Close()

	mu.Lock()
	log.Println(n, "closing")
}

func main() {
	log.Println("This is portfwd, forward any given tcp port to another machine")
	flag.Parse()

	if *flagPort <=0 {
		log.Fatalln("illegal port", *flagPort)
	}
	if *flagDest == "" {
		flag.Usage()
		log.Fatalln("missing destination, use portfwd -p 80 -s server.example.com:optional-port")
	}
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", *flagPort))
	if err != nil {
		log.Fatal(err)
	}

	n := 1000
	log.Println("Accepting connections on port", *flagPort)
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal(err)
		}
		n++
		go handleConnection(conn, *flagDest, *flagPort, n)
	}
}
